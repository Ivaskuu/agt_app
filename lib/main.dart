import 'package:agt/pages/home/home_page.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Advanced Guide for Tourists',
      theme: ThemeData(
        fontFamily: 'Google Sans',
        primaryColor: Colors.white,
        accentColor: Colors.black,
        primaryIconTheme: IconThemeData(color: Colors.black), // Appbar
        scaffoldBackgroundColor: Colors.white,
      ),
      home: HomePage(),
    );
  }
}
