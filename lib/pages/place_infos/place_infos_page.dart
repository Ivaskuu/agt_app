import 'dart:math';

import 'package:flutter/material.dart';

class PlaceInfosPage extends StatefulWidget {
  final String name;
  final String img;

  const PlaceInfosPage({Key key, @required this.name, @required this.img})
      : super(key: key);

  @override
  _PlaceInfosPageState createState() => _PlaceInfosPageState();
}

class _PlaceInfosPageState extends State<PlaceInfosPage> {
  bool showFullDesc = false;
  List<bool> showCategories = [true, true, true, true];
  Random rand = Random();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text('Place details'),
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.more_vert),
          ),
        ],
      ),
      body: ListView(
        children: <Widget>[
          SizedBox.fromSize(
            size: Size.fromHeight(400.0),
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                AspectRatio(
                  aspectRatio: 4 / 5,
                  child: Container(
                    margin:
                        EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                    child: Material(
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(16.0),
                      child:
                          Image.asset('res/${widget.img}', fit: BoxFit.cover),
                    ),
                  ),
                ),
                AspectRatio(
                  aspectRatio: 4 / 5,
                  child: Container(
                    margin:
                        EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                    child: Material(
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(16.0),
                      child: Image.asset('res/scala.jpg', fit: BoxFit.cover),
                    ),
                  ),
                ),
              ],
            ),
          ),
          ListTile(
            title: Text(
              '${widget.name}',
              style: TextStyle(
                color: Colors.black,
                fontSize: 26.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Icon(Icons.star, color: Colors.orange),
                Icon(Icons.star, color: Colors.orange),
                Icon(Icons.star, color: Colors.orange),
                Icon(Icons.star, color: Colors.orange),
                Icon(Icons.star, color: Colors.black26),
              ],
            ),
          ),
          ListTile(
            onTap: () {
              setState(() {
                showFullDesc = !showFullDesc;
              });
            },
            subtitle: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Text('🇮🇹'),
                    Text('🇺🇸'),
                    Text('🇫🇷'),
                    Text('🇨🇳'),
                    Text('🇷🇺'),
                    Text('🇦🇪'),
                  ],
                ),
                SizedBox(height: 12.0),
                Text(
                  '''"Duomo" è l'esito italiano (con dittongazione romanza ŏ > uo) del vocabolo latino domus che nell'antica Roma significava semplicemente casa.

Con le invasioni barbariche avvenne un cambio linguistico, per la sovrapposizione di termini stranieri sui termini latini o per il generale cambio delle condizioni di vita. La parola casa, che in latino significava "catapecchia" sostituì il significato della "domus", mentre le poche Domus romane sopravvissute, rimasero esempi stupendi non più ripetuti fino all'età romanica.

In quell'epoca il termine latino "Domus" venne riservato alla "Casa di Dio", cioè la chiesa principale di una comunità. Questo passaggio linguistico avvenne solo in Italia e in Germania: in altri paesi le chiese principali si chiamano in altro modo.

La maggior parte delle chiese importanti del medioevo, furono basiliche, perché si recuperarono le forme degli ex palazzi di giustizia romani, già molto diffusi. Tuttavia nei capoluoghi esistevano le costruzioni della ex Corte Imperiale che risaltavano sul resto dell'edilizia urbana, perché erano dotati di cupola. Questo elemento architettonico, grande novità del tardo impero, ebbe alto prestigio e fu portatore di significati e suggestioni legati al concetto di centralità del potere. Perciò il recupero delle costruzioni con cupola, per adattarle a chiese cristiane, creò un tipo di chiese ancor più prestigioso delle basiliche, e queste furono chiamate "duomo".

La cupola rimase un mito del medioevo, perché nessuno più sapeva come si potesse fare, fino attorno all'anno Mille, quando il grande risveglio comunale produsse un recupero di queste tecniche e tornarono ad esistere robusti edifici con soffitti a volta e cupole. Da allora, le principali costruzioni dotate di cupola presero la denominazione di "Dom", e "duomo" divenne così il sinonimo di cupola (infatti in inglese e in francese, per esempio, con "dome" si indicano comunemente le cupole).

Il duomo nelle città a capo di una diocesi era anche cattedrale, cioè chiesa dotata della "cattedra" vescovile. Nelle aree particolarmente ricche o attraversate da forti competizioni campanilistiche, ogni città volle il suo "duomo" e per questo non si deve confondere il termine duomo con cattedrale. Se il termine cattedrale presuppone che l'edificio sia anche "duomo", cioè chiesa maggiore, non vale invece il contrario: per esempio il duomo di Monza non è sede vescovile quindi non è una cattedrale. Esistono comunque delle eccezioni, come Molfetta, che ha il duomo di San Corrado e la cattedrale di Santa Maria Assunta, o anche Ragusa, che ha il suo duomo a Ibla e la cattedrale nella città nuova, oppure a Bologna dove la cattedrale non è la chiesa più famosa (la basilica di San Petronio). Ci possono essere anche due duomi in una stessa città, come nei casi di Brescia, con il duomo vecchio e il duomo nuovo, e di Caserta: il vecchio duomo sta nella vicina frazione di Casertavecchia e quello nuovo nell'omonima piazza.

Il termine duomo è estraneo al diritto canonico, pertanto è l'uso corrente a determinare se una chiesa possa chiamarsi duomo. Se la parola duomo non compare mai nella titolazione ufficiale che la diocesi dà a una chiesa, tale nome è registrato tuttavia, oltre che dall'uso comune, dalla toponomastica cittadina, mediante titolazioni quali "piazza del Duomo" e simili.''',
                  overflow: TextOverflow.fade,
                  maxLines: showFullDesc ? null : 6,
                ),
              ],
            ),
          ),
          AspectRatio(
            aspectRatio: 16 / 9,
            child: Container(
              margin: EdgeInsets.all(16.0),
              child: Material(
                elevation: 4.0,
                clipBehavior: Clip.antiAlias,
                borderRadius: BorderRadius.circular(16.0),
                child: Image.asset('res/map.jpg', fit: BoxFit.cover),
              ),
            ),
          ),
          _buildCategory(Icons.local_activity, 'Tickets', 0, [
            ListTile(
              dense: true,
              leading: Icon(Icons.people),
              title: Text('Adults (18+ years)'),
              trailing: Text(
                '10€',
                style: TextStyle(
                  color: Colors.green,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            ListTile(
              dense: true,
              leading: Icon(Icons.school),
              title: Text('Students (6-18 years)'),
              trailing: Text(
                '5€',
                style: TextStyle(
                  color: Colors.green,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            ListTile(
              dense: true,
              leading: Icon(Icons.face),
              title: Text('Children (0-6 years)'),
              trailing: Text(
                '3€',
                style: TextStyle(
                  color: Colors.green,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            ListTile(
              dense: true,
              leading: Icon(Icons.pregnant_woman),
              title: Text('Pregnant women'),
              trailing: Text(
                '20€',
                style: TextStyle(
                  color: Colors.green,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ]),
          SizedBox(height: 16.0),
          _buildCategory(Icons.schedule, 'Opening hours', 1, [
            ListTile(
              dense: true,
              title: Text('Monday'),
              trailing: Text(
                '09:00-12:00 13:00-18:00',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            ListTile(
              dense: true,
              title: Text('Tuesday - Friday'),
              trailing: Text(
                '09:00-18:00',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            ListTile(
              dense: true,
              title: Text('Saturday - Sunday'),
              trailing: Text(
                'Closed',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.italic,
                ),
              ),
            ),
          ]),
          SizedBox(height: 16.0),
          _buildCategory(Icons.rate_review, 'Reviews', 2, [
            ListTile(
              leading: CircleAvatar(
                backgroundImage: NetworkImage(
                  r'http://lh6.googleusercontent.com/-hkNa5Cv5KQ8/AAAAAAAAAAI/AAAAAAAAAAA/-h32N-nVe3I/w36-h36-p-k-mo/photo.jpg',
                ),
              ),
              title: Text('Giuseppi Reale ★★★★☆'),
              subtitle: Text(
                  'A very impressive, the well clean sweep, full of tourists.'),
            ),
            ListTile(
              leading: CircleAvatar(
                backgroundImage: NetworkImage(
                  r'http://lh4.googleusercontent.com/-P99azYst7YA/AAAAAAAAAAI/AAAAAAAAAAA/Nb96bm2XcA4/w36-h36-p-k-mo/photo.jpg',
                ),
              ),
              title: Text('Roberto Pucci ★★★☆☆'),
              subtitle:
                  Text('Cathedral beautiful, pity there is to pay admission.'),
            ),
            ListTile(
              leading: CircleAvatar(
                backgroundImage: NetworkImage(
                  r'http://lh3.googleusercontent.com/-Q6_pFGfMAAE/AAAAAAAAAAI/AAAAAAAAAAA/eRpu380VM84/w36-h36-p-k-mo/photo.jpg',
                ),
              ),
              title: Text('Stefano Schembari ★★★★★'),
              subtitle: Text(
                  'An exciting and wonderful place ... every time you see it, it\'s a magnificent thing'),
            ),
            FlatButton(
              onPressed: () {},
              child: ListTile(
                title: Text(
                  'TUTTE LE RECENSIONS (394)',
                  style: TextStyle(
                    color: Colors.blue,
                    // fontSize: 15.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                trailing: Icon(Icons.arrow_forward, color: Colors.blue),
              ),
            ),
          ]),
          SizedBox(height: 16.0),
          _buildCategory(Icons.remove_red_eye, 'Suggested for you', 3, [
            SizedBox.fromSize(
              size: Size.fromHeight(180.0),
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  _buildPlaceTile('Casa Alessandro Manzoni', 'manzoni.jpg'),
                  _buildPlaceTile('Navigli Milano', 'navigli.jpg'),
                  _buildPlaceTile('Trattoria Arlati', 'arlati.jpg'),
                  _buildPlaceTile('La Bicocca', 'bicocca.jpg'),
                ],
              ),
            ),
          ]),
        ],
      ),
    );
  }

  Widget _buildCategory(
      IconData icon, String title, int showPos, List<Widget> elements) {
    if (showCategories[showPos]) {
      return Column(
        children: elements
          ..insert(
            0,
            ListTile(
              onTap: () => setState(() => showCategories[showPos] = false),
              leading: Icon(icon, color: Colors.black),
              title: Text(
                title,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 26.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              trailing: Icon(Icons.keyboard_arrow_down),
            ),
          ),
      );
    } else
      return ListTile(
        onTap: () => setState(() => showCategories[showPos] = true),
        leading: Icon(icon, color: Colors.black),
        title: Text(
          title,
          style: TextStyle(
            color: Colors.black,
            fontSize: 26.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        trailing: Icon(Icons.keyboard_arrow_up),
      );
  }

  Widget _buildPlaceTile(String name, String img) {
    return AspectRatio(
      aspectRatio: 1.0,
      child: Container(
        margin: EdgeInsets.all(12.0),
        child: Material(
          clipBehavior: Clip.antiAlias,
          borderRadius: BorderRadius.circular(18.0),
          elevation: 6.0,
          shadowColor: Colors.black87,
          child: InkWell(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (_) => PlaceInfosPage(name: name, img: img)));
            },
            child: Stack(
              children: <Widget>[
                SizedBox.expand(
                  child: Image.asset('res/$img', fit: BoxFit.cover),
                ),
                SizedBox.expand(
                  child: Container(
                    color: Colors.black26,
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        name,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 22.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Icon(Icons.account_balance, color: Colors.white),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                (4.0 + rand.nextDouble()).toStringAsFixed(1),
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 22.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(width: 3.0),
                              Icon(Icons.star, color: Colors.white),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
