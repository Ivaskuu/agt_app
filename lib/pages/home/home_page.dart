import 'package:agt/pages/home/add_event_page.dart';
import 'package:agt/pages/home/dashboard_page.dart';
import 'package:agt/pages/home/map_page.dart';
import 'package:agt/pages/place_infos/place_infos_page.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentPage = 0;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.all(0.0),
          children: <Widget>[
            SizedBox.fromSize(
              size: Size.fromHeight(180.0),
              child: Stack(
                children: <Widget>[
                  SizedBox.expand(
                      child: Image.asset('res/duomo.jpg', fit: BoxFit.cover)),
                  SizedBox.expand(child: Container(color: Colors.black38)),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: ListTile(
                      leading: CircleAvatar(
                        backgroundImage: AssetImage('res/me.png'),
                      ),
                      title: Text(
                        'Hey, Tourist!',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 22.0,
                        ),
                      ),
                      subtitle: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          // SizedBox(height: 8.0),
                          Text(
                            '12/317 monuments visited at Milan',
                            style: TextStyle(
                              color: Colors.white,
                              // fontSize: 22.0,
                            ),
                          ),
                          Theme(
                            data: ThemeData(
                              accentColor: Colors.blue,
                              backgroundColor: Colors.white,
                            ),
                            child: LinearProgressIndicator(value: 0.1),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 16.0),
            ListTile(
              leading: Icon(Icons.account_circle),
              title: Text(
                'Account',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text(
                'Settings',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Divider(),
            ListTile(
              leading: Icon(Icons.info),
              title: Text(
                'App info',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              leading: Icon(Icons.star),
              title: Text(
                'Rate us 5 stars',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Divider(),
            ListTile(
              leading: Icon(Icons.exit_to_app, color: Colors.red),
              title: Text(
                'Log out',
                style:
                    TextStyle(fontWeight: FontWeight.bold, color: Colors.red),
              ),
            ),
          ],
        ),
      ),
      body: _currentPage == 0 ? DashBoardPage(_scaffoldKey) : MapPage(),
      floatingActionButton: _currentPage == 0
          ? FloatingActionButton.extended(
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (_) => AddEventPage()));
              },
              icon: Icon(_currentPage == 0 ? Icons.add_location : Icons.list),
              label: Text(
                  _currentPage == 0 ? 'Add a live event' : 'Live events list'),
            )
          : null,
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentPage,
        onTap: (i) {
          if (i == 2)
            scan();
          else
            setState(() => _currentPage = i);
        },
        fixedColor: Colors.black,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.map),
            title: Text('Map'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.camera_alt),
            title: Text('Scan QR'),
          ),
        ],
      ),
    );
  }

  Future scan() async {
    print('Scan');
    try {
      String barcode = await BarcodeScanner.scan();
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (_) => PlaceInfosPage()));
    } catch (e) {}
  }
}
