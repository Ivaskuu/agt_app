import 'package:agt/pages/place_infos/place_infos_page.dart';
import 'package:flutter/material.dart';
import 'dart:math';

class DashBoardPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  const DashBoardPage(this.scaffoldKey);

  @override
  _DashBoardPageState createState() => _DashBoardPageState();
}

class _DashBoardPageState extends State<DashBoardPage> {
  Random rand = Random();

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(horizontal: 16.0, vertical: 32.0),
          child: Material(
            color: Colors.white,
            elevation: 8.0,
            shadowColor: Colors.black54,
            clipBehavior: Clip.antiAlias,
            borderRadius: BorderRadius.circular(8.0),
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 4.0),
              child: Row(
                children: <Widget>[
                  IconButton(
                    onPressed: () {
                      widget.scaffoldKey.currentState.openDrawer();
                    },
                    icon: Icon(Icons.menu),
                  ),
                  Expanded(
                    child: TextField(
                      decoration: InputDecoration.collapsed(
                        hintText: 'Search for a place ...',
                      ),
                    ),
                  ),
                  IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.search),
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 16.0, vertical: 0.0)
              .copyWith(bottom: 16.0),
          child: Material(
            color: Colors.white,
            elevation: 8.0,
            shadowColor: Colors.black54,
            clipBehavior: Clip.antiAlias,
            borderRadius: BorderRadius.circular(8.0),
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 4.0),
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.access_time),
                    title: Text('Have lunch at Milan'),
                    subtitle: Text('See some recommended restaurants'),
                  ),
                  SizedBox(
                    height: 180.0,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: <Widget>[
                        _buildPlaceTile('Trattoria Arlati', 'arlati.jpg'),
                        _buildPlaceTile('La Bicocca', 'bicocca.jpg'),
                        _buildPlaceTile('La Cuccagnina', 'cuccagnina.jpg'),
                        _buildSeeMorePlaceTile(),
                      ],
                    ),
                  ),
                  ListTile(
                    leading: Icon(Icons.close),
                    title: Text('Ignore'),
                  ),
                ],
              ),
            ),
          ),
        ),
        ListTile(
          onTap: () {},
          leading: Icon(Icons.gps_fixed, color: Colors.black),
          title: Text(
            'Live events near you',
            style: TextStyle(
              color: Colors.black,
              fontSize: 26.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          trailing: Icon(Icons.arrow_forward, color: Colors.black),
        ),
        SizedBox(
          height: 180.0,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              _buildPlaceTile('Nuovo anno cinese', 'spett_cinesi.jpg'),
              _buildPlaceTile('Musica classica', 'spett_musica_classica.jpg'),
              _buildPlaceTile('Teatro', 'spett_teatro.jpg'),
              _buildSeeMorePlaceTile(),
            ],
          ),
        ),
        SizedBox(height: 12.0),
        ListTile(
          onTap: () {},
          leading: Icon(Icons.trending_up, color: Colors.black),
          title: Text(
            'Popular',
            style: TextStyle(
              color: Colors.black,
              fontSize: 26.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          trailing: Icon(Icons.arrow_forward, color: Colors.black),
        ),
        SizedBox(
          height: 180.0,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              _buildPlaceTile('Il Duomo', 'duomo.jpg'),
              _buildPlaceTile('Teatro La Scala', 'scala.jpg'),
              _buildPlaceTile('Casa Alessandro Manzoni', 'manzoni.jpg'),
              _buildPlaceTile('Navigli Milano', 'navigli.jpg'),
              _buildSeeMorePlaceTile(),
            ],
          ),
        ),
        SizedBox(height: 12.0),
        ListTile(
          onTap: () {},
          leading: Icon(Icons.account_balance, color: Colors.black),
          title: Text(
            'Museums',
            style: TextStyle(
              color: Colors.black,
              fontSize: 26.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          trailing: Icon(Icons.arrow_forward, color: Colors.black),
        ),
        SizedBox(
          height: 180.0,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              _buildPlaceTile('Il Duomo', 'duomo.jpg'),
              _buildPlaceTile('Teatro La Scala', 'scala.jpg'),
              _buildPlaceTile('Casa Alessandro Manzoni', 'manzoni.jpg'),
              _buildPlaceTile('Navigli Milano', 'navigli.jpg'),
              _buildSeeMorePlaceTile(),
            ],
          ),
        ),
        SizedBox(height: 12.0),
        ListTile(
          onTap: () {},
          leading: Icon(Icons.restaurant_menu, color: Colors.black),
          title: Text(
            'Locals',
            style: TextStyle(
              color: Colors.black,
              fontSize: 26.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          trailing: Icon(Icons.arrow_forward, color: Colors.black),
        ),
        SizedBox(
          height: 180.0,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              _buildPlaceTile('Trattoria Arlati', 'arlati.jpg'),
              _buildPlaceTile('La Bicocca', 'bicocca.jpg'),
              _buildPlaceTile('La Cuccagnina', 'cuccagnina.jpg'),
              _buildSeeMorePlaceTile(),
            ],
          ),
        ),
        SizedBox(height: 64.0),
      ],
    );
  }

  Widget _buildPlaceTile(String name, String img) {
    return AspectRatio(
      aspectRatio: 1.0,
      child: Container(
        margin: EdgeInsets.all(12.0),
        child: Material(
          clipBehavior: Clip.antiAlias,
          borderRadius: BorderRadius.circular(18.0),
          elevation: 6.0,
          shadowColor: Colors.black87,
          child: InkWell(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (_) => PlaceInfosPage(name: name, img: img)));
            },
            child: Stack(
              children: <Widget>[
                SizedBox.expand(
                  child: Image.asset('res/$img', fit: BoxFit.cover),
                ),
                SizedBox.expand(
                  child: Container(
                    color: Colors.black26,
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        name,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 22.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Icon(Icons.account_balance, color: Colors.white),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                (4.0 + rand.nextDouble()).toStringAsFixed(1),
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 22.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(width: 3.0),
                              Icon(Icons.star, color: Colors.white),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildSeeMorePlaceTile() {
    return AspectRatio(
      aspectRatio: 1.0,
      child: Container(
        margin: EdgeInsets.all(12.0),
        child: Material(
          color: Colors.black12,
          clipBehavior: Clip.antiAlias,
          borderRadius: BorderRadius.circular(18.0),
          child: InkWell(
            onTap: () {},
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.add),
                Text('Click to see more'),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
