import 'package:agt/pages/home/events_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

class MapPage extends StatefulWidget {
  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  MapController _controller = MapController();
  bool eventPreview = false;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        FlutterMap(
          mapController: _controller,
          options: MapOptions(
            center: LatLng(45.5135703, 9.2084312),
            zoom: 20.0,
            maxZoom: 22.0,
          ),
          layers: [
            TileLayerOptions(
              urlTemplate:
                  "https://api.mapbox.com/styles/v1/ivaskuu/cjp10qybf0ml22rt8acnnhpu2/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoiaXZhc2t1dSIsImEiOiJjam9ra291anowMGx1M3dvMnpqZXlrNWRtIn0.uB_uxywK746yo6MKyekL6g",
            ),
            MarkerLayerOptions(
              markers: [
                Marker(
                  anchor: AnchorPos.center,
                  width: 32.0,
                  height: 32.0,
                  point: LatLng(45.5135703, 9.2084312),
                  builder: (ctx) {
                    return CircleAvatar(
                      backgroundColor: Color(0xaaa5c7ff),
                    );
                  },
                ),
                Marker(
                  width: 13.0,
                  height: 13.0,
                  point: LatLng(45.5135703, 9.2084312),
                  builder: (ctx) {
                    return CircleAvatar(
                      backgroundColor: Colors.white,
                    );
                  },
                ),
                Marker(
                  width: 10.0,
                  height: 10.0,
                  point: LatLng(45.5135703, 9.2084312),
                  builder: (ctx) {
                    return CircleAvatar(
                      backgroundColor: Color(0xff3b72cc),
                    );
                  },
                ),
                Marker(
                  anchor: AnchorPos.center,
                  width: 48.0,
                  height: 48.0,
                  point: LatLng(45.5155703, 9.2084312),
                  builder: (ctx) {
                    return GestureDetector(
                      onTap: () {
                        _controller.move(
                            LatLng(45.5115703, 9.2084312), _controller.zoom);
                        setState(() {
                          eventPreview = true;
                        });
                      },
                      child: Icon(Icons.location_on,
                          color: Colors.purple, size: 40.0),
                    );
                  },
                ),
                Marker(
                    anchor: AnchorPos.center,
                    width: 48.0,
                    height: 48.0,
                    point: LatLng(45.5120903, 9.2093312),
                    builder: (ctx) {
                      return GestureDetector(
                        onTap: () {
                          _controller.move(
                              LatLng(45.5080903, 9.2093312), _controller.zoom);
                          setState(() {
                            eventPreview = true;
                          });
                        },
                        child: Icon(Icons.location_on,
                            color: Colors.amber, size: 40.0),
                      );
                    }),
                Marker(
                  anchor: AnchorPos.center,
                  width: 48.0,
                  height: 48.0,
                  point: LatLng(45.5125903, 9.2050312),
                  builder: (ctx) {
                    return GestureDetector(
                      onTap: () {
                        _controller.move(
                            LatLng(45.5085903, 9.2050312), _controller.zoom);
                        setState(() {
                          eventPreview = true;
                        });
                      },
                      child: Icon(Icons.location_on,
                          color: Colors.red, size: 40.0),
                    );
                  },
                ),
              ],
            ),
          ],
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 16.0, vertical: 32.0),
          child: Material(
            color: Colors.white,
            elevation: 8.0,
            shadowColor: Colors.black54,
            clipBehavior: Clip.antiAlias,
            borderRadius: BorderRadius.circular(8.0),
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 4.0),
              child: Row(
                children: <Widget>[
                  IconButton(
                    onPressed: () {
                      // widget.scaffoldKey.currentState.openDrawer();
                    },
                    icon: Icon(Icons.menu),
                  ),
                  Expanded(
                    child: TextField(
                      decoration: InputDecoration.collapsed(
                        hintText: 'Search for a place ...',
                      ),
                    ),
                  ),
                  IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.search),
                  ),
                ],
              ),
            ),
          ),
        ),
        eventPreview
            ? Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  margin:
                      EdgeInsets.symmetric(horizontal: 16.0, vertical: 32.0),
                  child: Material(
                    color: Colors.white,
                    elevation: 8.0,
                    shadowColor: Colors.black54,
                    clipBehavior: Clip.antiAlias,
                    borderRadius: BorderRadius.circular(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        SizedBox.fromSize(
                          size: Size.fromHeight(128.0),
                          child: Image.asset('res/spett_teatro.jpg',
                              fit: BoxFit.cover),
                        ),
                        ListTile(
                          title: Text(
                            'Theatre performance',
                            maxLines: 3,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 26.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Divider(),
                        ListTile(
                          dense: true,
                          leading: Icon(Icons.info),
                          title: Text('More information'),
                        ),
                        ListTile(
                          dense: true,
                          leading: Icon(Icons.share),
                          title: Text('Share'),
                        ),
                        ListTile(
                          onTap: () {
                            setState(() {
                              eventPreview = false;
                            });
                          },
                          dense: true,
                          leading: Icon(Icons.close),
                          title: Text('Close'),
                        ),
                        // IconButton(
                        //   onPressed: () {},
                        //   icon: Icon(Icons.search),
                        // ),
                      ],
                    ),
                  ),
                ),
              )
            : Align(
                alignment: Alignment.bottomRight,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: FloatingActionButton.extended(
                    onPressed: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (_) => EventsListPage()));
                    },
                    icon: Icon(Icons.list),
                    label: Text('Events list'),
                  ),
                ),
              ),
      ],
    );
  }
}
