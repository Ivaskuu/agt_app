import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

class AddEventPage extends StatefulWidget {
  @override
  _AddEventPageState createState() => _AddEventPageState();
}

class _AddEventPageState extends State<AddEventPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: Icon(Icons.close),
          )
        ],
        title: Text('Add live event'),
        elevation: 0.0,
      ),
      body: Container(
        margin: EdgeInsets.all(32.0),
        child: ListView(
          children: <Widget>[
            AspectRatio(
              aspectRatio: 16 / 9,
              child: Material(
                clipBehavior: Clip.antiAlias,
                borderRadius: BorderRadius.circular(16.0),
                color: Colors.black12,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Icon(Icons.add),
                    Text('Add an image'),
                  ],
                ),
              ),
            ),
            SizedBox(height: 32.0),
            TextField(
              decoration: InputDecoration(
                hintText: 'Event description',
              ),
            ),
            SizedBox(height: 32.0),
            Row(
              children: <Widget>[
                Text('Location:'),
                SizedBox(width: 16.0),
                Text(
                  'Not selected (click to open map)',
                  style: TextStyle(color: Colors.black45),
                ),
              ],
            ),
            SizedBox(height: 32.0),
            AspectRatio(
              aspectRatio: 16 / 9,
              child: Material(
                clipBehavior: Clip.antiAlias,
                borderRadius: BorderRadius.circular(16.0),
                color: Colors.black12,
                child: FlutterMap(
                  options: MapOptions(
                    center: LatLng(45.5135703, 9.2084312),
                    zoom: 15.0,
                  ),
                  layers: [
                    TileLayerOptions(
                      urlTemplate:
                          "https://api.mapbox.com/styles/v1/ivaskuu/cjp10qybf0ml22rt8acnnhpu2/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoiaXZhc2t1dSIsImEiOiJjam9ra291anowMGx1M3dvMnpqZXlrNWRtIn0.uB_uxywK746yo6MKyekL6g",
                    ),
                    MarkerLayerOptions(
                      markers: [
                        Marker(
                          anchor: AnchorPos.center,
                          width: 32.0,
                          height: 32.0,
                          point: LatLng(45.5135703, 9.2050312),
                          builder: (ctx) {
                            return CircleAvatar(
                              backgroundColor: Color(0xaaa5c7ff),
                            );
                          },
                        ),
                        Marker(
                          width: 13.0,
                          height: 13.0,
                          point: LatLng(45.5135703, 9.2050312),
                          builder: (ctx) {
                            return CircleAvatar(
                              backgroundColor: Colors.white,
                            );
                          },
                        ),
                        Marker(
                          width: 10.0,
                          height: 10.0,
                          point: LatLng(45.5135703, 9.2050312),
                          builder: (ctx) {
                            return CircleAvatar(
                              backgroundColor: Color(0xff3b72cc),
                            );
                          },
                        ),
                        Marker(
                          anchor: AnchorPos.center,
                          width: 48.0,
                          height: 48.0,
                          point: LatLng(45.5135703, 9.2084312),
                          builder: (ctx) {
                            return Icon(Icons.location_on,
                                color: Colors.red, size: 40.0);
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 48.0),
            Material(
              clipBehavior: Clip.antiAlias,
              borderRadius: BorderRadius.circular(16.0),
              color: Colors.black,
              elevation: 4.0,
              child: InkWell(
                onTap: () => Navigator.of(context).pop(),
                child: ListTile(
                  leading: Icon(Icons.add, color: Colors.white),
                  title: Text(
                    'Add live event',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
