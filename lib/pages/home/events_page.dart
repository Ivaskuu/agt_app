import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

class EventsListPage extends StatefulWidget {
  @override
  EventsListPageState createState() => EventsListPageState();
}

class EventsListPageState extends State<EventsListPage> {
  double sliderValue = 0.05;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text('Events list'),
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 16.0),
            child: Material(
              color: Colors.white,
              elevation: 4.0,
              shadowColor: Colors.black54,
              clipBehavior: Clip.antiAlias,
              borderRadius: BorderRadius.circular(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(16.0).copyWith(bottom: 0.0),
                    child: Text(
                      'Search range: ${sliderValue.toStringAsFixed(1)}km',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20.0,
                        // fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Slider(
                    activeColor: Colors.blue,
                    max: 10.0,
                    min: 0.1,
                    onChanged: (newValue) =>
                        setState(() => sliderValue = newValue),
                    value: sliderValue,
                  ),
                ],
              ),
            ),
          ),
          _buildEventTile(),
          _buildEventTile(),
          _buildEventTile(),
          _buildEventTile(),
          _buildEventTile(),
        ],
      ),
    );
  }

  Container _buildEventTile() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
      child: Material(
        color: Colors.white,
        elevation: 4.0,
        shadowColor: Colors.black54,
        clipBehavior: Clip.antiAlias,
        borderRadius: BorderRadius.circular(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox.fromSize(
              size: Size.fromHeight(128.0),
              child: IgnorePointer(
                child: FlutterMap(
                  options: MapOptions(
                    center: LatLng(45.5135703, 9.2084312),
                    zoom: 15.0,
                  ),
                  layers: [
                    TileLayerOptions(
                      urlTemplate:
                          "https://api.mapbox.com/styles/v1/ivaskuu/cjp10qybf0ml22rt8acnnhpu2/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoiaXZhc2t1dSIsImEiOiJjam9ra291anowMGx1M3dvMnpqZXlrNWRtIn0.uB_uxywK746yo6MKyekL6g",
                    ),
                    MarkerLayerOptions(
                      markers: [
                        Marker(
                          anchor: AnchorPos.center,
                          width: 32.0,
                          height: 32.0,
                          point: LatLng(45.5135703, 9.2084312),
                          builder: (ctx) {
                            return Icon(Icons.location_on,
                                color: Colors.amber, size: 40.0);
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            ListTile(
              title: Text('FESTIVAL'),
              subtitle: Text(
                'A group doing dances with fire',
                maxLines: 3,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Divider(),
            InkWell(
              onTap: () {},
              child: ListTile(
                leading: Icon(Icons.map, color: Colors.blue),
                title: Text(
                  'GO HERE',
                  maxLines: 3,
                  style: TextStyle(
                    color: Colors.blue,
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
